import User from "../Models/UserModel.js"
import createSecretToken from "../util/SecretToken.js"
import bcrypt from "bcrypt"

export default class AuthController {
  static async signup(req, res, next) {
    try {
      const { email, password, username, createdAt } = req.body; //user's inputs are obtained from the req.body
      const existingUser = await User.findOne({ email }); //check the email to make sure no past registrations have been made
      if (existingUser) {
        return res.json({ message: "User already exists" });
      }
      const user = await User.create({ email, password, username, createdAt });
      const token = createSecretToken(user._id); //MongoDB always assigns a new user with a unique _id. The newly formed user's _id is then supplied as an parameter to the createSecretToken() function, which handles token generation.
      res.cookie("token", token, {//The cookie will be sent to the client with key of "token", and value of token.
        withCredentials: true,
        httpOnly: false,
      });
      res
        .status(201)
        .json({ message: "User signed in successfully", success: true, user });
      next();
    } catch (error) {
      console.error(error);
    }

  }

  static async login(req, res, next){
    try {
      const { email, password } = req.body;
      if(!email || !password ){
        return res.json({message:'All fields are required'})
      }
      const user = await User.findOne({ email });
      if(!user){
        return res.json({message:'Incorrect password or email' }) 
      }
      const auth = await bcrypt.compare(password,user.password)
      if (!auth) {
        return res.json({message:'Incorrect password or email' }) 
      }
       const token = createSecretToken(user._id);
       res.cookie("token", token, {
         withCredentials: true,
         httpOnly: false,
       });
       res.status(201).json({ message: "User logged in successfully", success: true });
       next()
    } catch (error) {
      console.error(error);
    }
  }
}
