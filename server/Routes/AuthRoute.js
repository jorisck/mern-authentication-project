import AuthController from "../Controllers/AuthController.js"
import AuthMiddleware from "../Middlewares/AuthMiddleware.js"
import express from "express"

const router = express.Router()

router.post("/signup", AuthController.signup);
router.post('/login', AuthController.login)
router.post('/', AuthMiddleware.userVerification )

export default router