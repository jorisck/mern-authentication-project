import dotenv from "dotenv"
import jwt from "jsonwebtoken"
import User from "../Models/UserModel.js"

export default class AuthMiddleware {
    static async userVerification (req, res){ //The code checks if the user has access to the route by checking if the tokens match.
        const token = req.cookies.token
        if (!token) {
            return res.json({ status: false })
        }
        jwt.verify(token, process.env.TOKEN_KEY, async (err, data) => {
            if (err) {
                return res.json({ status: false })
            } else {
                const user = await User.findById(data.id)
                if (user) return res.json({ status: true, user: user.username })
                else return res.json({ status: false })
            }
        })
    }
}