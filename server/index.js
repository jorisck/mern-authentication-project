import app from "./server.js"
import mongoose  from "mongoose"
import dotenv from "dotenv"

dotenv.config()

const PORT = process.env.PORT ||  8000

mongoose.connect (
    process.env.MONGO_URL,
    {
        useNewUrlParser: true, //This property specifies that Mongoose should use the new URL parser to parse MongoDB connection strings. This is set to true by default.
        useUnifiedTopology: true, //This property specifies that Mongoose should use the new Server Discovery and Monitoring engine. This is set to false by default.
    }
)
.catch(err => {
    console.error(err.stack)
    process.exit(1)
})
.then( () => {
    console.log("MongoDB is  connected successfully")
    app.listen(PORT, () => {
        console.log(`Server is listening on port ${PORT}`);
      });
})

