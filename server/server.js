import express from "express"
import cors from "cors"
import cookieParser from "cookie-parser"
import authRoute from "./Routes/AuthRoute.js"


const app = express();

app.use(
    cors({
      origin: ["http://localhost:3000"],
      methods: ["GET", "POST", "PUT", "DELETE"],
      credentials: true,
    })
  );  //allow requests from other domains to access the resources on your server
app.use(cookieParser()); //The cookie-parser manages cookie-based sessions or extracts data from cookies. It's added to the code above along with the authRoute that the application will utilize.
app.use(express.json()) // our server can accept a json in the body of our request

app.use("/", authRoute);
app.use("*", (req, res) => res.status(404).json({error: "not found"}))

export default app 